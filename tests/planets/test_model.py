# -*- coding: utf-8 -*-

from mongoengine import IntField
from mongoengine import StringField
from modules.planets.models import Planet

class TestPlanet:

    def setup_method(self):
        self.data = {
            "name": "Alderaan",
            "terrain": "grasslands, mountains",
            "climate": "temperate",
            "appearances": 2,
        }

        # create a single instance of Planet model
        self.model = Planet(**self.data)

    def test_name_field_exists(self):
        """Check if the name field exists"""
        assert 'name' in self.model._fields

    def test_name_field_is_required(self):
        """Check if the name field is required"""
        assert self.model._fields['name'].required is True

    def test_name_field_is_unique(self):
        """Check if the name field is unique"""
        assert self.model._fields['name'].unique is True

    def test_name_field_is_str(self):
        """Check if the name field is a StringField"""
        assert isinstance(self.model._fields['name'], StringField)

    def test_terrain_field_exists(self):
        """Check if the terrain field exists"""
        assert 'terrain' in self.model._fields

    def test_terrain_field_is_required(self):
        """Check if the terrain field is required"""
        assert self.model._fields['terrain'].required is True

    def test_terrain_field_is_not_unique(self):
        """Check if the terrain field is not unique"""
        assert self.model._fields['terrain'].unique is False

    def test_terrain_field_is_str(self):
        """Check if the terrain field is a StringField"""
        assert isinstance(self.model._fields['terrain'], StringField)

    def test_climate_field_exists(self):
        """Check if the climate field exists"""
        assert 'climate' in self.model._fields

    def test_climate_field_is_required(self):
        """Check if the climate field is required"""
        assert self.model._fields['climate'].required is True

    def test_climate_field_is_not_unique(self):
        """Check if the climate field is not unique"""
        assert self.model._fields['climate'].unique is False

    def test_climate_field_is_str(self):
        """Check if the climate field is a StringField"""
        assert isinstance(self.model._fields['climate'], StringField)

    def test_appearances_field_exists(self):
        """Check if the appearances field exists"""
        assert 'appearances' in self.model._fields

    def test_appearances_field_is_not_required(self):
        """Check if the appearances field is required"""
        assert self.model._fields['appearances'].required is False

    def test_appearances_field_is_not_unique(self):
        """Check if the appearances field is not unique"""
        assert self.model._fields['appearances'].unique is False

    def test_appearances_field_is_int(self):
        """Check if the appearances field is a StringField"""
        assert isinstance(self.model._fields['appearances'], IntField)

    def test_all_fields_in_model(self):
        """Check if all fields exists in the model"""
        expected_fields = [
            "id", "name", "terrain", "climate", "appearances"
        ]

        model_fields = [k for k in self.model._fields.keys()]

        expected_fields.sort()
        model_fields.sort()

        assert expected_fields == model_fields