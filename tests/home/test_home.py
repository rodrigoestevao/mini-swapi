# -*- coding: utf-8 -*-

import json

from http import HTTPStatus as status

def test_index_response_200(client):
    response = client.get("/")
    assert response.status_code == status.OK