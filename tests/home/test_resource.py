# -*- coding: utf-8 -*-

import json

from http import HTTPStatus as status

def test_home_response_hello(client):
    """
    **Given** The user is accessing the API,
    **When**  He/She informs the route '/',
    **Then**  The API shall responds a object with the key `['hello']`,
    **And**   its content must be `world by modules`
    """
    resp = client.get("/")

    data = json.loads(resp.data.decode("utf-8"))

    assert data['hello'] == "world by modules"