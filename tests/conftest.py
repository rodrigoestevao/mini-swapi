# -*- coding: utf-8 -*-
import pytest

from os import getenv
from os.path import join
from os.path import isfile
from os.path import dirname
from os.path import realpath
from dotenv import load_dotenv

# define the .env path using the current file path
_ENV_FILE = realpath(join(dirname(__file__), '..', '.env'))

# if there is any .env file, load it
if isfile(_ENV_FILE):
    load_dotenv(dotenv_path=_ENV_FILE)

# creates a fixture that will be used in session scope

@pytest.fixture(scope="session")
def client():
    from modules import create_app

    # instantiate the app factory function
    app = create_app('testing')

    # get the testing path provided by Flask to be used by Werkzeud test Client
    testing_client = app.test_client()

    # before running the tests create the testing context using the application
    # context
    ctx = app.app_context()
    ctx.push()

    # returns the created client
    yield testing_client

    # remove from the context after the tests has been executed
    ctx.pop()

@pytest.fixture(scope="function")
def mongo(request, client):

    def fin():
        print('\n[teardown] disconnect from db')

    fin()