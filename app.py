# -*- coding: utf-8 -*-
from os import getenv
from os.path import join
from os.path import isfile
from os.path import dirname
from os.path import realpath
from dotenv import load_dotenv

# define the .env path using the current file path
_ENV_FILE = realpath(join(dirname(__file__), ".env"))

# if there is any .env file, load it
if isfile(_ENV_FILE):
    load_dotenv(dotenv_path=_ENV_FILE)

from modules import create_app

# MAIN ########################################################################

# instantiate the app factory function
app = create_app(getenv('FLASK_ENV', 'default'))

if __name__ == '__main__':
    ip = '0.0.0.0'
    port = app.config['PORT']
    debug = app.config['DEBUG']

    # start the Flask web server
    app.run(
        host=ip, debug=debug, port=port, use_reloader=debug
    )
