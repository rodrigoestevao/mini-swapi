# MINI SWAPI

This code was created to the Backend Developer position at B2W. It was developed at `Ubuntu 18.04 - 64 bits`, using the `Visual Studio Code` and consists on a REST API, created in `Python` and `Flask` and accessing a `MongoDb` database. The Python packages were installed via `Anaconda` and via `pip`.

## REQUIREMENTS

The following packages shall be installed via `conda` package manager:

~~~
conda install -c conda-forge requests flask flask-restful python-dotenv mongoengine pymongo Faker pytest pytest-flask pytest-cov pytest-runner marshmallow
~~~

The following package shall be installed via `pip` package manager:

~~~
pip install flask-mongoengine
~~~

> __NOTE:__  
> It is strongly recommended to create a new environment using the `environments.yml` file that comes with the package to create your test environment, thus you'll be using the same versions used at development phase.
   
~~~~
conda env create -f environment.yml
~~~~

## INSTALL

Before you run the tests you must install the application localy to fix any reference problem.

~~~~
pip install -e .
~~~~


## USAGE

The API provides the following functionalities:

* Create a Planet;  
* List all Planets;  
* Query a Planet by ID;  
* Query a Planet by Name;  

To start using it, go to API root dir and type the following command:

~~~~
python app.py
~~~~

If the API starts correclty you must see the messages like below:

~~~~
(swapi) [11:35:57 rodrigo@wakko ~/Workspace/mini-swapi]$ python app.py 
 * Serving Flask app "mini-swapi" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 198-150-629
~~~~

### ENDPOINTS

The table below has all the endpoints available for the application

Endpoint       | Methods     | Rule
---------------|-------------|-------------------------------
planets        | GET, POST   | /api/planets
planet_by_id   | DELETE, GET | /api/planets/i/<string:id>
planet_by_name | GET         | /api/planets/n/<string:name>

#### Create a new Planet

~~~~
curl http://0.0.0.0:5000/api/planets -H "Content-Type: application/json" -d '{"name": "Alderaan", "terrain": "grasslands, moutains", "climate": "temperate"}' -X POST -v
~~~~

#### Delete a Planet

~~~~
curl http://0.0.0.0:5000/api/planets/i/5d26405a60c048bb70cd11f9 -X DELETE -v
~~~~

#### List all Planets

~~~~
curl -v http://0.0.0.0:5000/api/planets
~~~~

#### Query a Planet by ID

~~~~
curl -v http://0.0.0.0:5000/api/planets/i/5d26405a60c048bb70cd11f9
~~~~

#### Query a Planet by Name

~~~~
curl -v http://0.0.0.0:5000/api/planets/n/Alderaan
~~~~