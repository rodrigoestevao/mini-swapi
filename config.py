# -*- coding: utf-8 -*-

from os import getenv
from secrets import token_hex


class Config:
    SECRET_KEY = getenv('SECRET_KEY', token_hex(24))
    PORT = int(getenv('PORT', '5000'))
    DEBUG = getenv('DEBUG', False)
    MONGODB_HOST = getenv('MONGODB_URI', 'mongodb://localhost:27017/planet')


class DevelopmentConfig(Config):
    FLASK_ENV = 'development'
    DEBUG = True


class TestingConfig(Config):
    FLASK_ENV = 'testing'
    TESTING = True
    MONGODB_HOST = getenv('MONGODB_URI_TEST')


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'default': DevelopmentConfig,
}
