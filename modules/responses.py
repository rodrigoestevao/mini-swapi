# -*- coding: utf-8 -*-

from http import HTTPStatus as status
from flask import jsonify
from .messages import MSG_EXCEPTION
from .messages import MSG_INVALID_DATA
from .messages import MSG_ALREADY_EXISTS
from .messages import MSG_NOT_FOUND
from .messages import MSG_RESOURCE_DELETED
from .messages import MSG_RESOURCE_CREATED


def _resp_builder(code, resource, message, **kwargs):
    """Generic response builder

    Parameters
    ----------
    code : int
        the HTTP status code
    resource : str
        the resource name
    messsage : str
        the response message

    Returns
    -------
    object
        a Response instance with the HTTP informed
    """
    payload = {
        "status": code,
        "resource": resource,
        "message": message,
    }

    payload.update(kwargs)

    resp = jsonify(payload)
    resp.status_code = code

    return resp


def resp_ok(resource, message, data=None, **kwargs):
    """Response 200 OK

    Parameters
    ----------
    resource : str
        the resource name
    messsage : str
        the response message
    data : object, optional
        data that shall be returned with the response (default is None)

    Returns
    -------
    object
        a Response instance with the HTTP status code `200 OK`
    """
    params = {"data": data} if data else {}

    params.update(kwargs)

    return _resp_builder(
        code=status.OK,
        resource=resource,
        message=message,
        **params
    )


def resp_invalid_data(resource, message=MSG_INVALID_DATA, **kwargs):
    """Response 422 Unprocessable Entity

    Parameters
    ----------
    resource : str
        the resource name
    messsage : str
        the response message

    Returns
    -------
    object
        a Response instance with the HTTP status code `422 Unprocessable
        Entity`
    """
    return _resp_builder(
        code=status.UNPROCESSABLE_ENTITY,
        resource=resource,
        message=message,
        **kwargs
    )


def resp_already_exists(resource, description):
    """Response 409 Conflict

    Parameters
    ----------
    resource : str
        the resource name
    description : str
        the description tha will complement the message

    Returns
    -------
    object
        a Response instance with the HTTP status code `409 Conflict`
    """
    return _resp_builder(
        code=status.CONFLICT,
        resource=resource,
        message=MSG_ALREADY_EXISTS.format(description)
    )


def resp_not_found(resource, description):
    """Response 404 Not Found

    Parameters
    ----------
    resource : str
        the resource name
    description : str
        the description tha will complement the message

    Returns
    -------
    object
        a Response instance with the HTTP status code `404 Not Found`
    """
    return _resp_builder(
        code=status.NOT_FOUND,
        resource=resource,
        message=MSG_NOT_FOUND.format(description)
    )


def resp_created(resource, location, **kwargs):
    """Response 201 Created

    Parameters
    ----------
    resource : str
        the resource name
    location : str
        a string representing the new resource location

    Returns
    -------
    object
        a Response instance with the HTTP status code `201 Created`
    """
    resp = _resp_builder(
        code=status.CREATED,
        resource=resource,
        message=MSG_RESOURCE_CREATED.format(resource)
    )

    resp.headers['location'] = location
    resp.autocorrect_location_header = False

    return resp


def resp_no_content(resource, description):
    """Response 204 No Content

    Parameters
    ----------
    resource : str
        the resource name
    description : str
        the description tha will complement the message

    Returns
    -------
    object
        a Response instance with the HTTP status code `204 No Content`
    """
    resp = _resp_builder(
        code=status.NO_CONTENT,
        resource=resource,
        message=MSG_RESOURCE_DELETED.format(description)
    )

    return resp


def resp_exception(resource, message=MSG_EXCEPTION, errors=None, **kwargs):
    """Response 500 Internal Server Error

    Parameters
    ----------
    resource : str
        the resource name
    errors : str, optional
        the original error messages or stack as text (default is None)

    Returns
    -------
    object
        a Response instance with the HTTP status code `500 Internal
        Server Error`
    """
    params = {"errors": errors} if errors else {}

    params.update(kwargs)

    return _resp_builder(
        code=status.INTERNAL_SERVER_ERROR,
        resource=resource,
        message=message,
        **params
    )
