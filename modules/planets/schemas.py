# -*- coding: utf-8 -*-

from bson import ObjectId
from marshmallow import Schema
from marshmallow.fields import Str
from marshmallow.fields import Int

from modules.messages import MSG_FIELD_REQUIRED


Schema.TYPE_MAPPING[ObjectId] = Str


class PlanetRegistrationSchema(Schema):
    """Planet schema used during the registration process

    Attributes
    ----------
    name : str
        Planet name
    terrain : str
        Planet terrain
    climate : str
        Planet climate
    appearances : int
        Number of appearances on movies (TODO: it should be dynamic!)
    """
    name = Str(
        required=True, error_messages={"required": MSG_FIELD_REQUIRED}
    )
    terrain = Str(
        required=True, error_messages={"required": MSG_FIELD_REQUIRED}
    )
    climate = Str(
        required=True, error_messages={"required": MSG_FIELD_REQUIRED}
    )
    appearances = Int()


class PlanetSchema(Schema):
    """Planet schema

    Attributes
    ----------
    id   : str
        Planet identifier
    name : str
        Planet name
    terrain : str
        Planet terrain
    climate : str
        Planet climate
    appearances : int
        Number of appearances on movies (TODO: it should be dynamic!)
    """
    # class Meta:
    #     model = models.Planet
    id = Str()
    name = Str(
        required=True, error_messages={"required": MSG_FIELD_REQUIRED}
    )
    terrain = Str(
        required=True, error_messages={"required": MSG_FIELD_REQUIRED}
    )
    climate = Str(
        required=True, error_messages={"required": MSG_FIELD_REQUIRED}
    )
    appearances = Int()
