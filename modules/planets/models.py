# -*- coding: utf-8 -*-

from mongoengine import IntField
from mongoengine import StringField

from modules.db import engine


class PlanetMixin(engine.Document):
    """Default implementation for Planet fields"""

    meta = {
        'abstract': True,
        'ordering': ['name']
    }

    name = StringField(required=True, unique=True)
    terrain = StringField(required=True)
    climate = StringField(required=True)
    appearances = IntField()


class Planet(PlanetMixin):
    """Planet model"""

    meta = {
        'collection': 'planets'
    }
