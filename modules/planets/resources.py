# -*- coding:utf-8 -*-

import requests

from flask import request
from flask_restful import Resource
from bson.objectid import ObjectId

from modules.responses import resp_ok
from modules.responses import resp_created
from modules.responses import resp_not_found
from modules.responses import resp_exception
from modules.responses import resp_no_content
from modules.responses import resp_invalid_data
from modules.responses import resp_already_exists
from modules.messages import MSG_RESOURCE_FETCHED
from modules.messages import MSG_RESOURCE_FETCHED_PAGINATED

from . import models
from .schemas import PlanetSchema
from .schemas import PlanetRegistrationSchema


class Planet(Resource):
    """Handle a individual Planet

    Methods
    -------
    find(id=None, name=None)
        Retrieves a single Planet from the collection
    get(id=None, name=None)
        Handle the GET request and retrieves a single Planet
    delete(id)
        Handle the DELETE requests and removes a Planet from collection
    """
    def __init__(self):
        self.resource = self.__class__.__name__
        self.model = models.Planet

    def get(self, id=None, name=None):
        """Handle the GET request and retrieves a single Planet from
        collection.

        Parameters
        ----------
        id : object, optional
            a string or ObjectId instance denoting the entity identifier
            (default is None)
        name : str, optional
            a string denoting the entity name (default is None)

        Returns
        -------
        object
            a json denoting the entity that has been found or None if
            no data is found
        """
        if id and id.strip():
            obj = self.model.objects(id=ObjectId(id))
        elif name and name.strip():
            obj = self.model.objects(name=name)
        else:
            obj = None
            res = resp_invalid_data(self.resource)

        if not obj:
            res = resp_not_found(self.resource, self.resource)
        else:
            schema = PlanetSchema()
            data = schema.dump(obj[0])
            description = f"Planet {data.get('name')}"
            res = resp_ok(
                self.resource,
                message=MSG_RESOURCE_FETCHED.format(description),
                data=data
            )

        return res

    def delete(self, id):
        """Handle the DELETE requests and removes a Planet from
        collection.

        Parameters
        ----------
        id : object, optional
            a string or ObjectId instance denoting the entity identifier
            (default is None)
        name : str, optional
            a string denoting the entity name (default is None)

        Returns
        -------
        object
            a json denoting the entity that has been found or None if
            no data is found
        """
        if id and id.strip():
            obj = self.model.objects(id=ObjectId(id))

            description = f"Planet identified by {id}"

            if obj:
                obj.delete()
                res = resp_no_content(self.resource, description)
            else:
                res = resp_not_found(self.resource, description)
        else:
            res = resp_invalid_data(self.resource)

        return res


class Planets(Resource):
    """Handle a collection of Planets.

    Methods
    -------
    get()
        Retrieves the hole collection of Planets
    post()
        Add a new Planet to collection if it does not exists yet.
    """
    def __init__(self):
        self.resource = self.__class__.__name__
        self.model = models.Planet
        self.urls = {
            "swapi": "http://swapi.co/api/planets",
        }

    def get(self, page_id=1):
        """Retrieves a list of all planets available

        Returns
        -------
        list
            A list of all Planets found on collection
        """
        schema = PlanetSchema(many=True)

        page_size = max(10, int(request.args.get("page_size", 10)))

        try:
            planets = self.model.objects.paginate(page_id, page_size)

            # Additional data to be sent with the response
            params = {
                'page': planets.page,
                'pages': planets.pages,
                'total': planets.total,
                'params': {'page_size': page_size}
            }

            # Dump the queried objects
            data = schema.dump(planets.items)

            if data:
                res = resp_ok(
                    self.resource,
                    message=MSG_RESOURCE_FETCHED_PAGINATED.format('planets'),
                    data=data,
                    **params
                )
            else:
                res = resp_not_found(self.resource, description=self.resource)
        except Exception as e:
            res = resp_exception(self.resource, errors=str(e))

        return res

    def post(self):
        """Saves the Planet extracted from the request on the collection

        Returns
        -------
        json
            A all Planets found on collection
        """

        res = None

        req_data = request.get_json()

        if not req_data:
            res = resp_invalid_data(self.resource)
        else:
            schema = PlanetRegistrationSchema()

            data = schema.load(req_data)

            name = data.get("name", None)

            if not name:
                res = resp_invalid_data(self.resource)
            else:
                planet = self.model.objects(name=name)

                if planet:
                    res = resp_already_exists(self.resource, self.resource)
                else:
                    # Retieves the complementar info from SWAPI and ensures the
                    # Planet is a valid planet on SW universe
                    resp = requests.get(
                        self.urls['swapi'], params={"search": name}
                    )

                    try:
                        resp.raise_for_status()

                        content = resp.json()

                        if content["count"] == 0:
                            res = resp_not_found(self.resource, self.resource)
                        else:
                            # Extract the planet info from results
                            info = content["results"][0]
                            data["appearances"] = len(info["films"])

                            planet = Planet(**data)
                            planet.save()

                            # Update the response with the new Planet address
                            location = '/'.join([
                                request.base_url, 'i', str(planet.id)
                            ])

                            res = resp_created(self.resource, location)

                    except Exception as e:
                        res = resp_exception(self.resource, errors=str(e))

        # Since the process have not been aborted, means that everythins is ok
        return res
