# -*- coding: utf-8 -*-

from flask import jsonify
from flask_restful import Api
from flask_restful import Resource

from modules.planets.resources import Planet
from modules.planets.resources import Planets


class Index(Resource):

    def get(self):
        return jsonify({"hello": "world by modules"})


# instantiate the Api
api = Api()


def configure_api(app):

    # simple route just for testing
    api.add_resource(
        Index,
        "/",
        endpoint="index"
    )

    # configure the planet routes
    api.add_resource(
        Planets,
        "/api/planets",
        endpoint="planets"
    )
    api.add_resource(
        Planet,
        "/api/planets/i/<string:id>",
        endpoint="planet_by_id"
    )
    api.add_resource(
        Planet,
        "/api/planets/n/<string:name>",
        endpoint="planet_by_name"
    )

    # initialize the API with the settings received as argument
    api.init_app(app)
