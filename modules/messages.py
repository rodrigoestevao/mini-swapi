# -*- coding: utf-8 -*-

MSG_FIELD_REQUIRED = 'Required Field.'
MSG_INVALID_DATA = 'Invalid input data.'
MSG_ALREADY_EXISTS = 'Already exists a {} with the given data.'
MSG_NOT_FOUND = '{} not found.'
MSG_EXCEPTION = 'Internal server error. Please contact the administrator.'
MSG_RESOURCE_FETCHED_PAGINATED = 'List with the {} paginted'

MSG_RESOURCE_CREATED = '{} created.'
MSG_RESOURCE_FETCHED = '{} retrieved.'
MSG_RESOURCE_UPDATED = '{} updated.'
MSG_RESOURCE_DELETED = '{} deleted.'
