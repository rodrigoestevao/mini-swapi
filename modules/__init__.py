# -*- coding: utf-8 -*-

from flask import Flask
from config import config

from .api import configure_api
from .db import engine

def create_app(config_name):
    app = Flask("mini-swapi")

    app.config.from_object(config[config_name])

    # configure MongoEngine
    engine.init_app(app)

    # configure the API
    configure_api(app)

    return app
